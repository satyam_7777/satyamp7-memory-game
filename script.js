const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "tomato",
  "tomato",
  "teal",
  "teal",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {

  let counter = 0;

  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);

    newDiv.classList.add("bg-transparent");
    newDiv.setAttribute("id", counter);

    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);
    timer();
    // append the div to the element with an id of game
    gameContainer.append(newDiv);
    counter += 1;
  }
}
function timer(){
  let sec = 0;
  function pad ( val ) { return val > 9 ? val : "0" + val; }
  setInterval( function(){
      document.getElementById("seconds").innerHTML= pad(++sec%60);
      document.getElementById("minutes").innerHTML=pad(parseInt(sec/60,10));
  }, 1000);
}

let previousClickedElement = null;
let clickedCards = [];
let globalAccess = false;
let clickCount=0;
let colorMatchCount=0;
function resetCards() {
  colorMatchCount = 0;
  clickCount=0;
  for(let i = 0;i < clickedCards.length; i += 1) {
    clickedCards[i].classList.add('bg-transparent');
  }
}


// TODO: Implement this function!
function handleCardClick(event) {

  // If setTimout is executing ignore click event
  if(globalAccess === true) {
    return;
  }
  

  if(event.target.classList.contains('bg-transparent') === true) {
    event.target.classList.remove('bg-transparent');
   
    clickCount++;
    document.getElementById("moves").innerHTML = "Moves:"+clickCount;
    clickedCards[parseInt(event.target.id)] = event.target;
  } else {
    return;
  }

  if(previousClickedElement !== null) {

    if(previousClickedElement.id !== event.target.id &&
        previousClickedElement.classList[0] === event.target.classList[0]) {

      previousClickedElement = null;
      colorMatchCount += 1;
      
      if(colorMatchCount === 5) {
        
        globalAccess = true;
       
        setTimeout(() => {
          document.getElementById("best").innerHTML = "Last Best:15"+clickCount;
          alert('You Won Moves:'+clickCount);
          resetCards();
          
        });

        globalAccess = false;
        
      }

    } else if(previousClickedElement.classList[0] !== event.target.classList[0]) {  
      
      globalAccess = true;

      setTimeout(() => {
  
        previousClickedElement.classList.add('bg-transparent');
        event.target.classList.add('bg-transparent');
        previousClickedElement = null;  
        globalAccess = false;

      }, 800); 
    } 
    
  } else {

    previousClickedElement = event.target;
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);

